#!/bin/bash
function menu() {
	echo "Digite a: Selecionar um arquivo"
	echo "Digite b: Preview do arquivo"
	echo "Digite c: Exibir o arquivo"
	echo "Digite d: Criar um arquivos com N linhas de blabla"
	echo "Digite e: Pesquisar algo no arquivo"
	echo "Digite f: Criar uma copia do arquivo celecionado"
	echo "Digite g: Sair"
}

while [ ! "$opc" = 'g' ]; do
	menu
	read -p 'Escolha uma opção: ' opc
	case $opc in
		a) read -p 'Escolha um arquivo: ' arq ;;
		b) if test -n "$arq"; then
			head -2 $arq
			tail -2 $arq
			fi	;;
		c) if test -n "$arq"; then
			cat $arq
			fi	;;
		d) if test -n "$arq"; then
			read -p 'Diga a quantidade de linhas do arquivo: ' linha; ./quest_1.sh $linha
			fi	;;
		e) if test -n "$arq"; then
			read -p 'Digite o que quer encontrar no arquivo: ' buscar;cat $arq | grep "$buscar"
			fi	;;
		f) if test -n "$arq"; then
			cp $arq $arq.bkp
			fi	;;
	esac
done
